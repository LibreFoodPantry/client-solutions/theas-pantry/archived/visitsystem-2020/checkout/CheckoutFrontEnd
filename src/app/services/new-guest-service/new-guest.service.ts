import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Guest} from '../../objects/guest';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DuplicateIdPopupComponent} from '../../duplicate-id-popup/duplicate-id-popup.component';
import {Subject} from 'rxjs';
import {GuestRemWeightCheck} from '../../objects/guest-rem-weight-check';
import {DelayService} from '../delay-service/delay.service';

@Injectable({
  providedIn: 'root'
})
export class NewGuestService {
  // this set of variables are subscribed to in the app component constructor,
  // and upon calling them, they deactivate the id-scanner component
  private appComponentSource = new Subject<null>();
  appComponent$ = this.appComponentSource.asObservable();

  error: HttpErrorResponse;
  registeredUrl: string = 'http://localhost:8080/';
  approvedUrl: string = 'http://localhost:8080/';
  restUrl: string = 'http://localhost:8080/';

  guests: Guest[] = [];
  currGuest: Guest;
  currGuestID: string;
  currGuestTot: number;
  currGuestRem: number;

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*'
    })
  };

  constructor(private snackBar: MatSnackBar, private http: HttpClient) {
  }

  setCurrGuest(guest: Guest): void {
    this.currGuest = guest;
    this.currGuestID = guest.studentID;
    this.currGuestRem = guest.remainingWeight;
    this.currGuestTot = guest.totalWeight;
  }

  clearCurrentGuest(): void {
    const index = this.guests.indexOf(this.currGuest);
    if (index > -1) {
      this.guests.splice(index, 1);
    }
    this.setCurrGuest(new Guest(null, null, null));
  }

  checkGuestRegistration(guestID: string): void {
    if (this.checkMatch(guestID)) {
      this.openSnackBar();
      return;
    }

    this.error = null;
    let isRegistered: boolean = false;
    this.http.get<boolean>(this.registeredUrl + 'registered/' + guestID, this.httpOptions).subscribe(response => {
        isRegistered = response;

        if (isRegistered) {
          this.checkGuestApproval(guestID);
        } else {
          console.log('This guest is not registered');
        }
      },
      (error: HttpErrorResponse) => {
        this.error = error;
      }
    );
  }

  checkGuestApproval(guestID: string) {
    this.error = null;
    let totWeight: number = 0;
    this.http.get<number>(this.approvedUrl + 'approved/' + guestID, this.httpOptions).subscribe(response => {
        totWeight = response;

        if (totWeight > 0) {
          this.getRemWeight(guestID, totWeight);
        }
      },
      (error: HttpErrorResponse) => {
        this.error = error;
      }
    );
  }

  getRemWeight(guestID: string, totWeight: number) {
    const remWeightCheck = this.buildRemWeightCheckObject(guestID);

    console.log('remWeightCheck\n\t' + remWeightCheck.getStudentID() + ' ' + remWeightCheck.getStartDate() + ' ' + remWeightCheck.getEndDate());
    console.log(remWeightCheck.getStudentID() + ' ' + remWeightCheck.getStartDate() + ' ' + remWeightCheck.getEndDate());

    // endpoint takes the guest ID plus the start and end date
    this.http.post(this.restUrl + 'checkouts/weight-taken-this-week', {
        studentID: remWeightCheck.getStudentID(),
        startDate: remWeightCheck.getStartDate(),
        endDate: remWeightCheck.getEndDate()
      }, this.httpOptions
    ).subscribe(data => {
        console.log('Calling collectTakenWeight');
        this.collectTakenWeight(guestID, totWeight);
      },
      (error: HttpErrorResponse) => {
        this.error = error;
      }
    );
  }

  buildRemWeightCheckObject(guestID: string): GuestRemWeightCheck {
    // gets the first and last day of the given week, weeks start on Monday and end on Sunday
    var date = new Date;
    var weekStart = new Date(date.setDate(date.getDate() - date.getDay() + 1));
    var weekEnd = new Date(date.setDate(weekStart.getDate() + 6));

    // build the guest-rem-weight-check object
    return new GuestRemWeightCheck(guestID, weekStart.toISOString().substring(0, 10), weekEnd.toISOString().substring(0, 10));
  }

  collectTakenWeight(guestID: string, totWeight: number): void {
    this.http.get<number>(this.restUrl + 'checkouts/collect-taken-weight', this.httpOptions).subscribe(data => {
        this.guests.push(new Guest(guestID, totWeight - data, totWeight));
        this.setCurrGuest(this.guests[this.guests.length - 1]);
        this.appComponentSource.next(null);
      }
    );
  }

  checkMatch(test: string): boolean {
    for (var val of this.guests) {
      if (test === val.studentID) {
        return true;
      }
    }
    return false;
  }

  logTransaction(guestID: string, weightTaken: number) {
    this.error = null;
    // @ts-ignore
    this.http.post(this.restUrl + 'checkouts/log-transaction/' + guestID + '/' + weightTaken).toPromise().then(r => console.log('success'));
  }

  openSnackBar(): void {
    this.snackBar.openFromComponent(DuplicateIdPopupComponent, {
      duration: 3000,
    });
  }
}
