import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UpdateGuestService {

  constructor(private http: HttpClient) {
  }

  updateWeight(transactionId: string, newWeight: number) : void {
    //TODO: Modify this endpoint as needed
    let endpointUrl = "http://localhost:8080/update_transaction/" + transactionId;
    let body = JSON.stringify({ newWeight: newWeight });

    this.http.post(endpointUrl, body, {})
      .toPromise().then(r => console.log('Weight successfully updated.'));
  }
}
