import {Component} from '@angular/core';
import {idscanWindowAnimation, weightEntryAnimation} from './animations/component-animations';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NewGuestService} from './services/new-guest-service/new-guest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './app-mobile.component.css'],
  animations: [idscanWindowAnimation, weightEntryAnimation]
})
export class AppComponent {
  title = 'thea\'s pantry';

  scannerOpen = false;
  weightEntryOpen = false;

  constructor(private _snackBar: MatSnackBar, public newGuestService: NewGuestService) {
    this.newGuestService.appComponent$.subscribe(res => {
      this.deactivateIDScanner();
    })
  }

  activateIDScanner(): void {
    this.deactivateWeightEntry();
    document.getElementById('main-container').style.transition = 'filter 200ms';
    document.getElementById('main-container').style.transitionDelay = '300ms';
    this.scannerOpen = true;
    document.getElementById('main-container').style.filter = 'blur(2px)';
  }

  deactivateIDScanner(): void {
    document.getElementById('main-container').style.transition = 'filter 200ms';
    document.getElementById('main-container').style.transitionDelay = '100ms';
    this.scannerOpen = false;
    document.getElementById('main-container').style.removeProperty('filter');
  }

  activateWeightEntry(): void {
    this.weightEntryOpen = true;
  }

  deactivateWeightEntry():void {
    this.weightEntryOpen = false;
  }
}
