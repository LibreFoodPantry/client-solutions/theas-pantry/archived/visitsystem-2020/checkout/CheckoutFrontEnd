import {animate, style, transition, trigger} from '@angular/animations';

export const idscanWindowAnimation = [
  trigger(
    'inOutVerticalAnimation',
    [
      transition(
        ':enter',
        [
          style({top: 0, opacity: 0}),
          animate('300ms cubic-bezier(.18,-0.02,.22,1.12)',
            style({top: '50%', opacity: 1}))
        ]
      ),
      transition(
        ':leave',
        [
          style({top: '50%', opacity: 1}),
          animate('200ms ease',
            style({top: 0, opacity: 0}))
        ]
      )
    ]
  )
];

export const weightEntryAnimation = [
  trigger(
    'inOutHorizontalAnimation',
    [
      transition(
        ':enter',
        [
          style({position: 'absolute', left: '-200%'}),
          animate(/*'500ms cubic-bezier(.18,-0.02,.22,1.12*/ '600ms cubic-bezier(.25,.36,.26,1.07)',
            style({position: 'relative', left: 0}))
        ]
      ),
      transition(
        ':leave',
        [
          style({position: 'relative', left: 0}),
          animate('300ms ease-in-out',
            style({position: 'absolute', left: '-200%'}))
        ]
      )
    ]
  )
];
